import turtle 

painter = turtle.Turtle()

painter.pencolor("blue")

n=40

for i in range(n):
    painter.forward(n)
    painter.left(123) # Let's go counterclockwise this time 
    
painter.pencolor("red")
for i in range(n):
    painter.forward(2*n)
    painter.left(123)
    
turtle.done()
